﻿//
//  Program.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define DEBUG

using System;
using System.Data;
using System.Data.Common;
using T4R.Data;
using System.Text;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Reflection;

namespace DBTest
{
    public class DumpStackTraceListener : TraceListener
    {
        public override void Write(string message)
        {
            Console.Write(message);
        }

        public override void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public override void Fail(string message)
        {
            Fail(message, String.Empty);
        }

        public override void Fail(string message1, string message2)
        {
            if (null == message2)
                message2 = String.Empty;

            Console.WriteLine("{0}: {1}", message1, message2);
            Console.WriteLine("Stack Trace:");

            StackTrace trace = new StackTrace(true);
            foreach (StackFrame frame in trace.GetFrames())
            {
                MethodBase frameClass = frame.GetMethod();
                Console.WriteLine("  {2}.{3} {0}:{1}",
                                   frame.GetFileName(),
                                   frame.GetFileLineNumber(),
                                   frameClass.DeclaringType,
                                   frameClass.Name);
            }

#if DEBUG
            Console.WriteLine("Exiting because Fail");
            Environment.Exit(1);
#endif
        }
    }

    class MainClass
    {
        public static void Main(string[] args)
        {
#if DEBUG
            Debug.Listeners.Add(new DumpStackTraceListener());
#endif

            //PostgresLexer parser = new PostgresLexer();
            //parser.Parse("x'ABCD1234'");

            //DBConnection conn = new DBConnection("GameOfLife");
            DBConnection conn = new DBConnection("TestDB");
            //DBConnection conn = new DBConnection ("TestMysql");
            //DBConnection conn = new DBConnection("TestPostgres");

            //PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Session");
            PreparedStatement stmt = conn.prepareStatement("   INSERT INTO Inventory(id, name, quantity) VALUES (3, 'hi', 0);");
            //PreparedStatement stmt = conn.prepareStatement("SELECT *, '' AS A FROM Inventory WHERE id >= $1;", 0);
            //PreparedStatement stmt = conn.prepareStatement("SELECT *, '' AS A FROM Inventory WHERE id >= :1;", 0);
            //PreparedStatement stmt = conn.prepareStatement("SELECT *, '' AS A FROM Inventory WHERE id >= ?;", 0);

            int insertVal = stmt.executeInsert();
            //ResultSet rs = stmt.executeQuery();

            //while (rs.MoveNext())
            //{
            //    for (int i = 0; i < rs.fieldCount(); i++)
            //    {
            //        object val = rs.fetchFieldData(i);
            //        System.Console.Out.WriteLine("i: " + i + " val: \"" + val.ToString() + "\"");
            //    }
            //}

            return;
        }
    }
}
