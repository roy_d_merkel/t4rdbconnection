﻿//
//  DBConnection.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using MySql.Data.MySqlClient;
using Npgsql;
using System.Xml;
using System.Xml.XPath;

namespace T4R.Data
{
    public class DBConnection : IDisposable
    {
        private bool disposed = false;
        private string clazz = null;
        private string server = null;
        private uint port = 0;
        private string database = null;
        private bool connected = false;
        internal DbConnection conn = null;
        internal LinkedList<PreparedStatement> stmts = new LinkedList<PreparedStatement>();

        static readonly string[] tomcatSearchPaths = new string[] { "var/lib/tomcat/conf/context.xml", "var/lib/tomcat5/conf/conf/context.xml", "var/lib/tomcat6/conf/conf/context.xml", "var/lib/tomcat7/conf/conf/context.xml", "var/lib/tomcat8/conf/conf/context.xml", "var/lib/tomcat9/conf/conf/context.xml", "etc/tomcat/conf/context.xml", "etc/tomcat5/conf/context.xml", "etc/tomcat6/conf/context.xml", "etc/tomcat7/conf/context.xml", "etc/tomcat8/conf/context.xml", "etc/tomcat9/conf/context.xml" };

        private static string RemoveWhitespace(string str)
        {
            return string.Join("", str.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
        }

        static private bool ParseMysqlUrlString(string url, out string host, out string port, out string db)
        {
            int idx = 0;

            bool hasColon = false;
            bool hasSlash = false;
            bool foundIdx = false;

            host = "";
            port = "3306";
            db = "";

            foreach (char c in url)
            {
                idx++;
                if (c == ':' && !hasColon)
                {
                    hasColon = true;
                }
                else if (Char.IsWhiteSpace(c))
                {
                }
                else if (c == '/')
                {
                    if (hasColon && !hasSlash)
                    {
                        hasSlash = true;
                    }
                    else if (hasColon && hasSlash)
                    {
                        foundIdx = true;
                        break;
                    }
                    else
                    {
                        hasColon = false;
                        hasSlash = false;
                    }
                }
                else
                {
                    hasColon = false;
                    hasSlash = false;
                }
            }

            if (!foundIdx)
            {
                return false;
            }

            string curUrl = url.Substring(idx);
            bool doHost = true;
            bool doPort = false;
            bool doDb = false;
            bool parseFailed = false;

            foreach (char c in curUrl)
            {
                if (doHost)
                {
                    if (c == ':')
                    {
                        doHost = false;
                        doPort = true;
                        port = "";
                    }
                    else if (c == '/')
                    {
                        doHost = false;
                        doDb = true;
                    }
                    else
                    {
                        host += c;
                    }
                }
                else if (doPort)
                {
                    if (doPort && c == '/')
                    {
                        doPort = false;
                        doDb = true;
                    }
                    else
                    {
                        port += c;
                    }
                }
                else if (doDb)
                {
                    if (c == '/')
                    {
                        parseFailed = true;
                        break;
                    }
                    else if (c == '?')
                    {
                        break;
                    }
                    else
                    {
                        db += c;
                    }
                }
            }

            return !parseFailed;
        }

        static private bool ParsePostgresqlUrlString(string url, out string host, out string port, out string db)
        {
            int idx = 0;

            bool hasColon = false;
            bool hasSlash = false;
            bool foundIdx = false;

            host = "";
            port = "5432";
            db = "";

            foreach (char c in url)
            {
                idx++;
                if (c == ':' && !hasColon)
                {
                    hasColon = true;
                }
                else if (Char.IsWhiteSpace(c))
                {
                }
                else if (c == '/')
                {
                    if (hasColon && !hasSlash)
                    {
                        hasSlash = true;
                    }
                    else if (hasColon && hasSlash)
                    {
                        foundIdx = true;
                        break;
                    }
                    else
                    {
                        hasColon = false;
                        hasSlash = false;
                    }
                }
                else
                {
                    hasColon = false;
                    hasSlash = false;
                }
            }

            if (!foundIdx)
            {
                return false;
            }

            string curUrl = url.Substring(idx);
            bool doHost = true;
            bool doPort = false;
            bool doDb = false;
            bool parseFailed = false;

            foreach (char c in curUrl)
            {
                if (doHost)
                {
                    if (c == ':')
                    {
                        doHost = false;
                        doPort = true;
                        port = "";
                    }
                    else if (c == '/')
                    {
                        doHost = false;
                        doDb = true;
                    }
                    else
                    {
                        host += c;
                    }
                }
                else if (doPort)
                {
                    if (doPort && c == '/')
                    {
                        doPort = false;
                        doDb = true;
                    }
                    else
                    {
                        port += c;
                    }
                }
                else if (doDb)
                {
                    if (c == '/')
                    {
                        parseFailed = true;
                        break;
                    }
                    else if (c == '?')
                    {
                        break;
                    }
                    else
                    {
                        db += c;
                    }
                }
            }

            return !parseFailed;
        }

        static private bool ParseSqlServerUrlString(string url, out string host, out string port, out string db, ref string userName, ref string password)
        {
            int idx = 0;

            bool hasColon = false;
            bool hasSlash = false;
            bool foundIdx = false;

            host = "";
            port = "1433";
            db = "";

            foreach (char c in url)
            {
                idx++;
                if (c == ':' && !hasColon)
                {
                    hasColon = true;
                }
                else if (Char.IsWhiteSpace(c))
                {
                }
                else if (c == '/')
                {
                    if (hasColon && !hasSlash)
                    {
                        hasSlash = true;
                    }
                    else if (hasColon && hasSlash)
                    {
                        foundIdx = true;
                        break;
                    }
                    else
                    {
                        hasColon = false;
                        hasSlash = false;
                    }
                }
                else
                {
                    hasColon = false;
                    hasSlash = false;
                }
            }

            if (!foundIdx)
            {
                return false;
            }

            string curUrl = url.Substring(idx);
            bool doHost = true;
            bool doPort = false;
            bool parseFailed = false;

            idx = 0;

            foreach (char c in curUrl)
            {
                idx++;
                if (doHost)
                {
                    if (c == ':')
                    {
                        doHost = false;
                        doPort = true;
                        port = "";
                    }
                    else if (c == '/')
                    {
                        parseFailed = true;
                        break;
                    }
                    else if (c == ';')
                    {
                        break;
                    }
                    else
                    {
                        host += c;
                    }
                }
                else if (doPort)
                {
                    if (doPort && c == '/')
                    {
                        parseFailed = true;
                        break;
                    }
                    else if (c == ';')
                    {
                        break;
                    }
                    else
                    {
                        port += c;
                    }
                }
            }

            if (parseFailed)
            {
                return false;
            }

            curUrl = curUrl.Substring(idx);

            bool getKey = true;
            bool getValue = false;
            bool getQuoteValue = false;
            bool escaped = false;

            string key = "";
            string value = "";
            Dictionary<string, string> properties = new Dictionary<string, string>();

            foreach (char c in curUrl)
            {
                if (getKey)
                {
                    if (c == ';')
                    {
                        parseFailed = true;
                        break;
                    }
                    else if (c == '/')
                    {
                        parseFailed = true;
                        break;
                    }
                    else if (c == '=')
                    {
                        getKey = false;
                        getValue = true;
                    }
                    else
                    {
                        key += c;
                    }
                }
                else if (getValue)
                {
                    if (c == ';')
                    {
                        getKey = true;
                        getValue = false;
                        properties.Add(key, value);
                        key = "";
                        value = "";
                    }
                    else if (c == '"')
                    {
                        getValue = false;
                        getQuoteValue = true;
                    }
                    else
                    {
                        value += c;
                    }
                }
                else if (getQuoteValue)
                {
                    if (escaped)
                    {
                        escaped = false;
                        value += c;
                    }
                    else if (c == '"')
                    {
                        getValue = true;
                        getQuoteValue = false;
                    }
                    else if (c == '\\')
                    {
                        escaped = true;
                    }
                    else
                    {
                        value += c;
                    }
                }
            }

            if (getQuoteValue)
            {
                return false;
            }
            else if (getValue && key != "" && value != "")
            {
                properties.Add(key, value);
                key = "";
                value = "";
            }

            foreach (KeyValuePair<string, string> kv in properties)
            {
                switch (kv.Key.ToLower())
                {
                    case "databasename":
                        {
                            if (!db.Equals(""))
                            {
                                return false;
                            }

                            db = kv.Value;
                        }
                        break;
                    case "user":
                        {
                            if (!userName.Equals("") && userName != null && !userName.Equals(kv.Value))
                            {
                                return false;
                            }

                            userName = kv.Value;
                        }
                        break;
                    case "password":
                        {
                            if (!password.Equals("") && password != null && !password.Equals(kv.Value))
                            {
                                return false;
                            }

                            password = kv.Value;
                        }
                        break;
                }
            }

            return true;
        }

        static private DbConnection ConnectWithContextImpl(out string outClazz, out string outServer, out uint outPort, out string outDatabase, string context)
        {
            DriveInfo[] drives = DriveInfo.GetDrives();
            List<string> searchPathsList = new List<string>();
            List<bool> useTomcatsMapList = new List<bool>();
            string xpathApache = "/Context/Resource[@name='" + context + "']";
            string xpathTomcat = "/Context/Resource[@name='" + "jdbc/" + context + "']";

            string[] searchPaths = new string[] { };
            bool[] useTomcatsMap = new bool[] { };
            XPathDocument doc = null;

            outClazz = null;
            outServer = null;
            outPort = 0;
            outDatabase = null;

            foreach (DriveInfo drive in drives)
            {
                string drivePath = drive.ToString();
                bool useTomcat = false;

                string searchPath = "";
                if (drivePath.EndsWith("/") || drivePath.EndsWith("\\"))
                {
                    searchPath = drivePath.ToString() + "var/wwwContext/context.xml";
                }
                else
                {
                    searchPath = drivePath.ToString() + "/var/wwwContext/context.xml";
                }

                searchPathsList.Add(searchPath);
                useTomcatsMapList.Add(useTomcat);

                useTomcat = true;
                foreach (string tomcatSearchPath in tomcatSearchPaths)
                {
                    if (drivePath.EndsWith("/") || drivePath.EndsWith("\\"))
                    {
                        searchPath = drivePath.ToString() + tomcatSearchPath;
                    }
                    else
                    {
                        searchPath = drivePath.ToString() + "/" + tomcatSearchPath;
                    }

                    searchPathsList.Add(searchPath);
                    useTomcatsMapList.Add(useTomcat);
                }
            }

            searchPaths = searchPathsList.ToArray();
            useTomcatsMap = useTomcatsMapList.ToArray();

            for (int i = 0; i < searchPaths.GetLength(0); i++)
            {
                try
                {
                    XPathDocument d = new XPathDocument(searchPaths[i]);

                    doc = d;
                }
                catch (FileNotFoundException)
                {
                }
                catch (DirectoryNotFoundException)
                {
                }

                if (doc != null)
                {
                    XPathNavigator nav = doc.CreateNavigator();
                    XPathNodeIterator nodeIter = null;

                    if (useTomcatsMap[i])
                    {
                        nodeIter = nav.Select(xpathTomcat);
                        while (nodeIter.MoveNext())
                        {
                            if (nodeIter.Current != null && nodeIter.Current.HasAttributes)
                            {
                                string clazz = nodeIter.Current.GetAttribute("driverClassName", "");
                                string userName = nodeIter.Current.GetAttribute("username", "");
                                string password = nodeIter.Current.GetAttribute("password", "");
                                string url = nodeIter.Current.GetAttribute("url", "");
                                string nospaceurl = RemoveWhitespace(url.ToLower());

                                switch (clazz.ToLower())
                                {
                                    case "com.mysql.jdbc.driver":
                                    case "org.gjt.mm.mysql.driver":
                                        if (nospaceurl.StartsWith("jdbc:mysql:"))
                                        {
                                            string host;
                                            string port;
                                            string db;

                                            if (ParseMysqlUrlString(url, out host, out port, out db))
                                            {
                                                uint iport = 0;

                                                if (uint.TryParse(RemoveWhitespace(port), out iport))
                                                {
                                                    return ConnectWithUsernamePasswordServerImpl(out outClazz, out outServer, out outPort, out outDatabase, "mysql", host, db, userName, password, iport);
                                                }
                                            }
                                        }
                                        break;
                                    case "org.postgresql.driver":
                                        if (nospaceurl.StartsWith("jdbc:postgresql:"))
                                        {
                                            string host;
                                            string port;
                                            string db;

                                            if (ParsePostgresqlUrlString(url, out host, out port, out db))
                                            {
                                                uint iport = 0;

                                                if (uint.TryParse(RemoveWhitespace(port), out iport))
                                                {
                                                    return ConnectWithUsernamePasswordServerImpl(out outClazz, out outServer, out outPort, out outDatabase, "postgresql", host, db, userName, password, iport);
                                                }
                                            }
                                        }
                                        break;
                                    case "com.microsoft.sqlserver.jdbc.sqlserverdriver":
                                        if (nospaceurl.StartsWith("jdbc:sqlserver:"))
                                        {
                                            string host;
                                            string port;
                                            string db;

                                            if (ParseSqlServerUrlString(url, out host, out port, out db, ref userName, ref password))
                                            {
                                                uint iport = 0;

                                                if (uint.TryParse(RemoveWhitespace(port), out iport))
                                                {
                                                    return ConnectWithUsernamePasswordServerImpl(out outClazz, out outServer, out outPort, out outDatabase, "sqlserver", host, db, userName, password, iport);
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        outClazz = "Unknown: tomcat: " + clazz;
                                        break;
                                }
                            }
                        }
                    }
                    else
                    {
                        nodeIter = nav.Select(xpathApache);
                        while (nodeIter.MoveNext())
                        {
                            if (nodeIter.Current != null && nodeIter.Current.HasAttributes)
                            {
                                string clazz = nodeIter.Current.GetAttribute("class", "");
                                string userName = nodeIter.Current.GetAttribute("username", "");
                                string password = nodeIter.Current.GetAttribute("password", "");
                                string host = nodeIter.Current.GetAttribute("host", "");
                                string db = nodeIter.Current.GetAttribute("db", "");
                                string port = nodeIter.Current.GetAttribute("port", "");
                                uint uport = 0;

                                if (port == null || port.Equals("") || !uint.TryParse(port, out uport) || uport == 0)
                                {
                                    return ConnectWithUsernamePasswordServerImpl(out outClazz, out outServer, out outPort, out outDatabase, clazz, host, db, userName, password);
                                }
                                else
                                {
                                    return ConnectWithUsernamePasswordServerImpl(out outClazz, out outServer, out outPort, out outDatabase, clazz, host, db, userName, password, uport);
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }

        static private DbConnection ConnectWithUsernamePasswordServerImpl(out string outClazz, out string outServer, out uint outPort, out string outDatabase, string driverClassName, string server, string database, string userName, string password, uint port = 0)
        {
            outClazz = null;
            outServer = null;
            outPort = 0;
            outDatabase = null;

            switch (driverClassName.ToLower())
            {
                case "mysql":
                case "mysqli":
                case "mysqlnd":
                    {
                        outClazz = "mysql";
                        MySqlConnectionStringBuilder connStrBuilder = new MySqlConnectionStringBuilder();
                        connStrBuilder.Database = database;
                        connStrBuilder.Server = server;

                        if (port > 0)
                        {
                            outPort = port;
                            connStrBuilder.Port = port;
                        }
                        else
                        {
                            outPort = 3306;
                        }
                        connStrBuilder.UserID = userName;
                        connStrBuilder.Password = password;
						connStrBuilder.SslMode = MySqlSslMode.None;
						connStrBuilder.AllowZeroDateTime = true;
						connStrBuilder.ConvertZeroDateTime = true;

                        string connStr = connStrBuilder.GetConnectionString(true);

                        DbConnection conn = new MySqlConnection(connStr);

                        if (conn != null)
                        {
                            try
                            {
                                conn.Open();
                            }
                            catch (Exception exc)
                            {
                                conn = null;
                            }

                            if (conn == null)
                            {
                                return null;
                            }

                            while (conn.State == ConnectionState.Connecting)
                            {
                                Thread.Yield();
                            }

                            if (conn.State == ConnectionState.Broken || conn.State == ConnectionState.Closed)
                            {
                                return null;
                            }

                            return conn;
                        }

                        break;
                    }
                case "pgsql":
                case "postgresql":
                case "postgres":
                    {
                        outClazz = "postgresql";
                        NpgsqlConnectionStringBuilder connStrBuilder = new NpgsqlConnectionStringBuilder();
                        connStrBuilder.Database = database;
                        connStrBuilder.Host = server;
                        if (port > 0)
                        {
                            outPort = port;
                            connStrBuilder.Port = checked((int)port);
                        }
                        else
                        {
                            outPort = 5432;
                        }
                        outServer = server;
                        outDatabase = database;

                        connStrBuilder.Username = userName;
                        connStrBuilder.Password = password;

                        string connStr = connStrBuilder.ConnectionString;

                        DbConnection conn = new NpgsqlConnection(connStr);

                        if (conn != null)
                        {
                            try
                            {
                                conn.Open();
                            }
                            catch (Exception)
                            {
                                conn = null;
                            }

                            if (conn == null)
                            {
                                return null;
                            }

                            while (conn.State == ConnectionState.Connecting)
                            {
                                Thread.Yield();
                            }

                            if (conn.State == ConnectionState.Broken || conn.State == ConnectionState.Closed)
                            {
                                return null;
                            }

                            return conn;
                        }

                        break;
                    }
                case "sqlserver":
                case "sqlsrv":
                    {
                        outClazz = "sqlserver";
                        SqlConnectionStringBuilder connStrBuilder = new SqlConnectionStringBuilder();
                        if (port == 0)
                        {
                            outPort = 1433;
                            connStrBuilder.DataSource = server;
                        }
                        else
                        {
                            outPort = port;
                            connStrBuilder.DataSource = server + "," + port.ToString();
                        }
                        outServer = server;
                        outDatabase = database;

                        connStrBuilder.InitialCatalog = database;
                        connStrBuilder.UserID = userName;
                        connStrBuilder.Password = password;

                        string connStr = connStrBuilder.ConnectionString;

                        DbConnection conn = new SqlConnection(connStr);

                        if (conn != null)
                        {
                            try
                            {
                                conn.Open();
                            }
                            catch (Exception)
                            {
                                conn = null;
                            }

                            if (conn == null)
                            {
                                return null;
                            }

                            while (conn.State == ConnectionState.Connecting)
                            {
                                Thread.Yield();
                            }

                            if (conn.State == ConnectionState.Broken || conn.State == ConnectionState.Closed)
                            {
                                return null;
                            }

                            return conn;
                        }

                        break;
                    }
                default:
                    outClazz = "unknown: " + driverClassName;
                    break;
            }
            return null;
        }

        public bool IsConnected { get { return connected; } }
        public string DriverClass { get { if (!connected) { return null; } else { return clazz; } } }
        public string Server { get { if (!connected) { return null; } else { return server; } } }
        public uint? Port { get { if (!connected) { return null; } else { return port; } } }
        public string Database { get { if (!connected) { return null; } else { return database; } } }

        public DBConnection()
        {
            this.clazz = null;
            this.connected = false;
            this.conn = null;
            this.server = null;
            this.port = 0;
            this.database = null;
        }

        public DBConnection(string context)
        {
            string outClazz = null;
            string outServer = null;
            uint outPort = 0;
            string outDatabase = null;
            DbConnection outConn = DBConnection.ConnectWithContextImpl(out outClazz, out outServer, out outPort, out outDatabase, context);

            if (outConn != null)
            {
                this.conn = outConn;
                this.clazz = outClazz;
                this.server = outServer;
                this.port = outPort;
                this.database = outDatabase;
                this.connected = true;
            }
        }

        public DBConnection(string driverClassName, string server, string database, string userName, string password, uint port = 0)
        {
            string outClazz = null;
            string outServer = null;
            uint outPort = 0;
            string outDatabase = null;
            DbConnection outConn = DBConnection.ConnectWithUsernamePasswordServerImpl(out outClazz, out outServer, out outPort, out outDatabase, driverClassName, server, database, userName, password, port);

            if (outConn != null)
            {
                this.conn = outConn;
                this.clazz = outClazz;
                this.server = outServer;
                this.port = outPort;
                this.database = outDatabase;
                this.connected = true;
            }
        }

        public bool Connect(string context)
        {
            string outClazz = null;
            string outServer = null;
            uint outPort = 0;
            string outDatabase = null;
            DbConnection outConn = DBConnection.ConnectWithContextImpl(out outClazz, out outServer, out outPort, out outDatabase, context);

            this.disposed = false;

            if (outConn != null)
            {
                this.conn = outConn;
                this.clazz = outClazz;
                this.server = outServer;
                this.port = outPort;
                this.database = outDatabase;
                this.connected = true;

                return true;
            }

            return false;
        }

        public bool Connect(string driverClassName, string server, string database, string userName, string password, uint port = 0)
        {
            string outClazz = null;
            string outServer = null;
            uint outPort = 0;
            string outDatabase = null;
            DbConnection outConn = DBConnection.ConnectWithUsernamePasswordServerImpl(out outClazz, out outServer, out outPort, out outDatabase, driverClassName, server, database, userName, password, port);

            this.disposed = false;

            if (outConn != null)
            {
                this.conn = outConn;
                this.clazz = outClazz;
                this.server = outServer;
                this.port = outPort;
                this.database = outDatabase;
                this.connected = true;

                return true;
            }

            return false;
        }


        // Returns null on failure or an object on success.
        public PreparedStatement prepareStatement(string stmt, params ParamType[] parameters)
        {
            PreparedStatement prepStmt = null;

            if (stmt != null)
            {
                prepStmt = new PreparedStatement(this, stmt, parameters);
            }

            return prepStmt;
        }

        public List<Dictionary<string, object>> ExecuteQuery(string stmt, params ParamType[] parameters)
        {
            PreparedStatement ps = null;
            List<Dictionary<string, object>> res = new List<Dictionary<string, object>>();

            try
            {
                ps = prepareStatement(stmt, parameters);

                if (ps != null)
                {
                    ResultSet rs = null;

                    try
                    {
                        rs = ps.executeQuery();

                        if (rs != null)
                        {
                            while (rs.MoveNext())
                            {
                                Dictionary<string, object> row = new Dictionary<string, object>();

                                for (int i = 0; i < rs.fieldCount(); i++)
                                {
                                    row.Add(rs.fetchFieldMetaData(i).Value.Name, rs.fetchFieldData(i));
                                }

                                res.Add(row);
                            }
                        }
                    }
                    finally
                    {
                        if (rs != null)
                        {
                            rs.Close();
                            rs = null;
                        }
                    }
                }
            }
            finally
            {
                if (ps != null)
                {
                    ps.Close();
                    ps = null;
                }
            }

            return res;
        }

        public int ExecuteInsert(string stmt, params ParamType[] parameters)
        {
            PreparedStatement ps = null;
            int res = -1;

            try
            {
                ps = prepareStatement(stmt, parameters);

                if (ps != null)
                {
                    res = ps.executeInsert();
                }
            }
            finally
            {
                if (ps != null)
                {
                    ps.Close();
                    ps = null;
                }
            }

            return res;
        }

        public int ExecuteUpdate(string stmt, params ParamType[] parameters)
        {
            PreparedStatement ps = null;
            int res = -1;

            try
            {
                ps = prepareStatement(stmt, parameters);

                if (ps != null)
                {
                    res = ps.executeUpdate();
                }
            }
            finally
            {
                if (ps != null)
                {
                    ps.Close();
                    ps = null;
                }
            }

            return res;
        }

        public void Close()
        {
            if (connected)
            {
                if (conn != null && conn.State != ConnectionState.Broken && conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }

                if (conn != null)
                {
                    conn.Dispose();
                }
            }

            conn = null;
            connected = false;
            server = null;
            port = 0;
            database = null;
            clazz = null;
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    Close();
                }

                // Note disposing has been done.
                disposed = true;
            }
        }

        ~DBConnection()
        {
            Close();
        }

        private static string ParseAndTransformQueryString(string query)
        {
            return null;
        }
    }
}
