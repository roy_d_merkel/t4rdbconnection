﻿//
//  PreparedStatement.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Text.RegularExpressions;

namespace T4R.Data
{
    public struct ParamType
    {
        public Type paramType;
        public object param;

        public ParamType(Type paramType, object param)
        {
            this.paramType = paramType;
            this.param = param;
        }
    }

    public class PreparedStatement : IDisposable
    {
        private bool disposed = false;
        private DBConnection db = null;
        internal DbCommand cmd = null;
        private ParamType[] @params = null;
        internal LinkedList<ResultSet> resultSets = new LinkedList<ResultSet>();

        public PreparedStatement(DBConnection db, string stmt, params ParamType[] parms)
        {
            if (!this.Prepare(db, stmt, parms))
            {
                throw new ApplicationException("Failed to prepare statement.");
            }
        }

        public bool Prepare(DBConnection db, string stmt, params ParamType[] parms)
        {
            if (cmd != null)
            {
                Dispose();
            }

            disposed = false;

            if (db == null)
            {
                throw new ArgumentNullException("db is null.");
            }
            else if (stmt == null)
            {
                throw new ArgumentNullException("stmt is null.");
            }

            this.db = db;
            cmd = db.conn.CreateCommand();

            if (cmd == null)
            {
                return false;
            }

            cmd.CommandText = stmt;

            for (int i = 0; i < parms.Length; i++)
            {
                DbParameter parm = cmd.CreateParameter();
                ParamType objparm = parms[i];

                parm.ParameterName = (i + 1).ToString();

                if (objparm.paramType == typeof(bool) || objparm.paramType == typeof(Boolean))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.Boolean;
                }
                else if (objparm.paramType == typeof(char) || objparm.paramType == typeof(Char))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.Int16;
                }
                else if (objparm.paramType == typeof(string) || objparm.paramType == typeof(String))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.String;
                }
                else if (objparm.paramType == typeof(byte) || objparm.paramType == typeof(Byte))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.Byte;
                }
                else if (objparm.paramType == typeof(sbyte) || objparm.paramType == typeof(SByte))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.SByte;
                }
                else if (objparm.paramType == typeof(ushort) || objparm.paramType == typeof(UInt16))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.UInt16;
                }
                else if (objparm.paramType == typeof(short) || objparm.paramType == typeof(Int16))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.Int16;
                }
                else if (objparm.paramType == typeof(uint) || objparm.paramType == typeof(UInt32))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.UInt32;
                }
                else if (objparm.paramType == typeof(int) || objparm.paramType == typeof(Int32))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.Int32;
                }
                else if (objparm.paramType == typeof(ulong) || objparm.paramType == typeof(UInt64))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.UInt64;
                }
                else if (objparm.paramType == typeof(long) || objparm.paramType == typeof(Int64))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
                    parm.DbType = DbType.Int64;
                }
				else if (objparm.paramType == typeof(decimal) || objparm.paramType == typeof(Decimal))
                {
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = objparm.param;
                    }
					parm.DbType = DbType.Decimal;
                }
                else if (objparm.paramType == typeof(DateTime))
                {
                    parm.DbType = DbType.DateTime;
                    parm.Direction = ParameterDirection.Input;

                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = ((DateTime)objparm.param).ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                }
                else if (objparm.paramType == typeof(Date))
                {
                    parm.DbType = DbType.Date;
                    parm.Direction = ParameterDirection.Input;
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = ((Date)objparm.param).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }
                }
                else if (objparm.paramType == typeof(Time))
                {
                    parm.DbType = DbType.Time;
                    parm.Direction = ParameterDirection.Input;
                    if (objparm.param == null)
                    {
                        parm.Value = DBNull.Value;
                    }
                    else
                    {
                        parm.Value = ((Time)objparm.param).ToString("HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                }
                else
                {
                    throw new NotImplementedException("No prepared statement support for the following type: " + objparm.paramType.ToString());
                }

                cmd.Parameters.Add(parm);
            }

            this.@params = parms;

            db.stmts.AddLast(this);

            return true;
        }

        public ResultSet executeQuery()
        {
            return new ResultSet(this);
        }

        /*private static readonly string ws = @"[ \t\r\n]";
        private static readonly string nws = @"[^ \t\r\n]";
        private static readonly string with = "(?:WITH" + ws + "+(?:RECURSIVE" + ws + "+)?" + nws + "+(?:" + ws + "*," + ws + "*" + nws + "+)*" + ws + "+)";
        private static readonly Regex reg = new Regex("^[ \t\r\n]*(" + with + "?)INSERT((?:" + ws + "+INTO|" + ws + "+LOW_PRIORITY|" + ws + "+DELAYED|" + ws + "+HIGH_PRIORITY|" + ws + "+IGNORE|" + ws + "+TOP" + ws + "*[(]" + ws + "*[@]?[^ \\t\\r\\n)]+" + ws + "*[)](?:" + ws + "*PERCENT)?)*)" + ws + "+([^\r\n\t (]+)", RegexOptions.IgnoreCase);*/
        public int executeInsert()
        {
            // farm the table name.
            /*Match mat = reg.Match(this.cmd.CommandText);
            string table = mat.Groups[3].Value;
            string query = null;
            string[] keys = new string[0];*/

            // use reflection to get the primary keys.
            /*switch (this.db.DriverClass)
            {
                case "mysql":
                    query =
                        @"";
                    break;
                case "postgresql":
                    query =
@"";
                    break;
                case "sqlserver":
                    query =
                        @"SELECT KU.table_name as TABLENAME,column_name as PRIMARYKEYCOLUMN
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
INNER JOIN
    INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
          ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
             TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME AND 
             KU.table_name=@1
ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;";
                    DbCommand reflect = this.db.conn.CreateCommand();
                    reflect.CommandText = query;
                    DbParameter tableName = reflect.CreateParameter();
                    tableName.ParameterName = "1";
                    tableName.Value = table;
                    reflect.Parameters.Add(tableName);

                    DbDataReader reader = reflect.ExecuteReader();

                    List<string> keysList = new List<string>();
                    if (reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            keysList.Add(reader.GetString(1));
                        }
                    }

                    reader.Close();

                    reflect.Dispose();

                    keys = keysList.ToArray();

                    break;
                default:
                    throw new NotSupportedException("driver class: " + this.db.DriverClass + " is not supported.");
            }*/

            return this.cmd.ExecuteNonQuery();
        }

        public int executeUpdate()
        {
            return this.cmd.ExecuteNonQuery();
        }

        public void Close()
        {
            if (cmd != null)
            {
                cmd.Dispose();

                cmd = null;
            }

            if (db != null)
            {
                db.stmts.Remove(this);

                db = null;
            }
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    Close();
                }

                // Note disposing has been done.
                disposed = true;
            }
        }

        ~PreparedStatement()
        {
            Close();
        }
    }
}
