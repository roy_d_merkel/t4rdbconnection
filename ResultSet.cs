﻿//
//  ResultSet.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.IO;
using System.Data.Common;
using MySql.Data.Types;

namespace T4R.Data
{
    public struct MetaData
    {
        public string Name;
        public Type Type;
    }

    public class ResultSet : IDisposable
    {
        private bool disposed = false;
        private PreparedStatement stmt = null;
        private DbDataReader reader = null;
        private MetaData[] metaData = null;
        private bool hasRow = false;

        public ResultSet(PreparedStatement stmt)
        {
            this.stmt = stmt;

            this.stmt.resultSets.AddLast(this);

            reader = stmt.cmd.ExecuteReader();

            if (reader == null)
            {
                throw new InvalidDataException("Prepared Statement is an update or a delete not a select.");
            }

            int fieldCount = reader.FieldCount;

            metaData = new MetaData[fieldCount];

            for (int i = 0; i < fieldCount; i++)
            {
                metaData[i].Name = reader.GetName(i);
                metaData[i].Type = reader.GetFieldType(i);

                if (metaData[i].Type == typeof(DateTime))
                {
                    metaData[i].Type = typeof(DateTime?);
                }
                else if (metaData[i].Type == typeof(Date))
                {
                    metaData[i].Type = typeof(Date?);
                }
                else if (metaData[i].Type == typeof(Time))
                {
                    metaData[i].Type = typeof(Time?);
                }
            }

            hasRow = reader.HasRows;
        }

        // returns 0 on invalid or no fields or the field count.
        public int fieldCount()
        {
            if (reader == null)
            {
                return 0;
            }
            else
            {
                return reader.FieldCount;
            }
        }

        // fetches null or the field metadata.
        public MetaData? fetchFieldMetaData(int i)
        {
            MetaData? result = null;

            if (reader != null)
            {
                result = this.metaData[i];
            }

            return result;
        }

        // fetches null or the field's value.
        public object fetchFieldData(int i)
        {
            if (reader != null)
            {
                if (hasRow)
                {
                    object val = reader.GetValue(i);
                    
					if (metaData[i].Type == typeof(MySqlDateTime))
					{
						DateTime? dtret = null;

						if (val is DBNull)
						{
							return dtret;
						}
						else if (val == null)
						{
							return dtret;
						}
						else if (val is DateTime && (DateTime)val == DateTime.MinValue)
						{
							return dtret;
						}
						else if (val is DateTime)
						{
							dtret = (DateTime)val;
							return dtret;
						}
						else if (((MySqlDateTime)val).IsNull)
						{
							return dtret;
						}
						else
						{
							dtret = (DateTime)((MySqlDateTime)val).GetDateTime();
							return dtret;
						}
					}
                    else if (metaData[i].Type == typeof(DateTime?))
                    {
                        DateTime? dtret = null;
                        if (val is DBNull)
                        {
                            return dtret;
                        }
                        else if (val == null)
                        {
                            return dtret;
                        }
                        else
                        {
                            dtret = (DateTime)val;
							return dtret;
                        }
                    }
                    else if (metaData[i].Type == typeof(Date?))
                    {
                        Date? dtret = null;
                        if (val is DBNull)
                        {
                            return dtret;
                        }
                        else if (val == null)
                        {
                            return dtret;
                        }
                        else
                        {
                            dtret = (Date)val;
							return dtret;
                        }
                    }
                    else if (metaData[i].Type == typeof(Time?))
                    {
                        Time? dtret = null;
                        if (val is DBNull)
                        {
                            return dtret;
                        }
                        else if (val == null)
                        {
                            return dtret;
                        }
                        else
                        {
                            dtret = (Time)val;
							return dtret;
                        }
                    }
                    else
                    {
                        return reader.GetValue(i);
                    }
                }
            }

            return null;
        }

        public bool MoveNext()
        {
            if (hasRow)
            {
                hasRow = reader.Read();

                return hasRow;
            }

            return false;
        }

        public void Close()
        {
            if (reader != null)
            {
                reader.Close();
            }

            if (stmt != null)
            {
                stmt.resultSets.Remove(this);

                stmt = null;
            }

            metaData = null;
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    Close();
                }

                // Note disposing has been done.
                disposed = true;
            }
        }

        ~ResultSet()
        {
            Close();
        }
    }
}
